public class MaterialPoint2D extends Point2D {
  private int mass;
  public MaterialPoint2D(double v, double v1, int mass) {
    super(v,v1);
    this.mass = mass;
  }

  public int getMass(){
    return mass;
  }

  public String toString(){

    return ("X = " + getX() + " Y = " + getY() + " mass = " + mass);
  }
}
