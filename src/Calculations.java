public class Calculations {
  public static Point2D positionGeometricCenter(Point2D[] points) {
     double sumx=0.0;
     double sumy=0.0;
    for (Point2D point : points){
      sumx += point.getX();
      sumy += point.getY();
    }
    sumx= sumx/ points.length;
    sumy= sumy/ points.length;
    return new Point2D(sumx, sumy);
  }

  public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoints) {
    double sumx=0.0;
    double sumy=0.0;
    int mass=0;
    for (MaterialPoint2D point : materialPoints){
      sumx += point.getX() * point.getMass();
      sumy += point.getY() * point.getMass();
      mass+= point.getMass();
    }
    sumx= sumx/ mass;
    sumy= sumy/ mass;
    return new MaterialPoint2D(sumx, sumy, mass);
  }
}
